package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int lines = getNumberOfLines(inputNumbers.size());

        /* this is typical matrix:
			{{0, 0, 1, 0, 0},
			{0, 2, 0, 3, 0},
			{4, 0, 5, 0, 9}};
		    in last(n) line we have n values -> number of columns = n * 2 - 1;
		 */
        int columns = 2 * lines - 1;

        int[][] pyramid = new int[lines][columns];
        Collections.sort(inputNumbers);

        Queue<Integer> queue = new LinkedList<>(inputNumbers);
        int firstIndexToInsertInLine = columns / 2;

        for (int i = 0; i < lines; i++) {
            int currentIndex = firstIndexToInsertInLine;

            // count of symbols in line = number of line + 1
            for (int j = 0; j <= i; j ++ ) {
                pyramid[i][currentIndex] = queue.remove();
                currentIndex += 2;
            }
            firstIndexToInsertInLine--;
        }
        return pyramid;
    }

    /*
     * Method for computing the number of rows
     * We can build pyramid only if we have total size of input numbers, such that:
     * 1, 3, 6, 10, ...
     * We got it from:
     * first row 1 element sum = 1
     * second row 2 elements, sum = 3 = (2+1)
     * third row 3 elements, sum = 6 = (3+3)
     * forth row 4 elements, sum = 10 = (6+4) ...
     * Else its impossible to fill all gaps in Pyramid Matrix ???
     * So we have arithmetic progression -> and SUM of it = SIZE = n(n+1)/2, where n = number of rows
     *
     * n(n+1)/2 = SIZE  -> two roots:
     * n0 = (-1 + sqrth(1 + 8*SIZE))/2
     * n1 = (-1 - sqrth(1 + 8*SIZE))/2
     * Use only positive integer root
     */

    private int getNumberOfLines(int size) throws CannotBuildPyramidException {
        double result = (-1 + Math.sqrt(1 + 8 * size)) / 2;
        if (result % 1 == 0) {
            return (int) result;
        }
        throw new CannotBuildPyramidException();
    }
}
