package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class ListOfElements {
    private final ArrayList<Element> ELEMENTS;

    public ListOfElements() {
        ELEMENTS = new ArrayList();
    }

    public boolean isEmpty() {
        return ELEMENTS.size() == 0;
    }

    public Element getTop() {
        return ELEMENTS.get(ELEMENTS.size() - 1);
    }

    public void addElement(Element t) {
        ELEMENTS.add(t);
    }

    public void removeElement() {
        ELEMENTS.remove(ELEMENTS.size() - 1);
    }
}
