package com.tsystems.javaschool.tasks.calculator;

public class Element {
    public static final int SPACE = 0; // if you think that space also invalid this line should be deleted or commented out
    public static final int DIGIT = 1;
    public static final int MATH_SYMBOL = 2;
    public static final int LEFT_PARENTHESIS = 3;
    public static final int RIGHT_PARENTHESIS = 4;
    public static final int INVALID = -1;

    private int type;
    private double value;
    private char mathSymbol;
    private int precedence;

    public Element(String contents) {
        switch (contents) {
            case "+":
                type = MATH_SYMBOL;
                mathSymbol = contents.charAt(0);
                precedence = 1;
                break;
            case "-":
                type = MATH_SYMBOL;
                mathSymbol = contents.charAt(0);
                precedence = 1;
                break;
            case "*":
                type = MATH_SYMBOL;
                mathSymbol = contents.charAt(0);
                precedence = 2;
                break;
            case "/":
                type = MATH_SYMBOL;
                mathSymbol = contents.charAt(0);
                precedence = 2;
                break;
            case "(":
                type = LEFT_PARENTHESIS;
                break;
            case ")":
                type = RIGHT_PARENTHESIS;
                break;
            // if you think that space is also invalid next case block should be deleted or commented out
            case " ":
                type = SPACE;
                break;
            default:
                type = DIGIT;
                try {
                    value = Double.parseDouble(contents);
                } catch (Exception e) {
                    type = INVALID;
                }
        }
    }

    public Element(double number) {
        type = DIGIT;
        value = number;
    }

    int getType() {
        return type;
    }

    double getValue() {
        return value;
    }

    int getPrecedence() {
        return precedence;
    }

    Element calculate(double x, double y) throws Exception {
        double result = 0;
        switch (mathSymbol) {
            case '+':
                result = x + y;
                break;
            case '-':
                result = x - y;
                break;
            case '*':
                result = x * y;
                break;
            case '/':
                if (y == 0) {
                    throw new Exception();
                }
                result = x / y;
                break;
        }
        return new Element(result);
    }
}
