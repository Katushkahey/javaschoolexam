package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private final ListOfElements OPERATOR_STACK;
    private final ListOfElements VALUE_STACK;
    private boolean error;

    public Calculator() {
        OPERATOR_STACK = new ListOfElements();
        VALUE_STACK = new ListOfElements();
        error = false;
    }

    private void intermediateCalculation(Element element) {
        Element X, Y;
        if (VALUE_STACK.isEmpty()) {
            error = true;
            return;
        } else {
            Y = VALUE_STACK.getTop();
            VALUE_STACK.removeElement();
        }
        if (VALUE_STACK.isEmpty()) {
            error = true;
            return;
        } else {
            X = VALUE_STACK.getTop();
            VALUE_STACK.removeElement();
        }
        try {
            Element tempResult = element.calculate(X.getValue(), Y.getValue());
            VALUE_STACK.addElement(tempResult);
        } catch (Exception e) {
            System.out.println("Division by zero");
            error = true;
        }
    }

    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }
        char[] chars = statement.toCharArray();
        ArrayList<String> elementsList = new ArrayList<>();
        StringBuilder s = new StringBuilder();

        for (char aChar : chars) {
            if (Character.isDigit(aChar) || aChar == '.') {
                s.append(aChar);
            } else {
                if (!s.toString().equals("")) {
                    elementsList.add(s.toString());
                    s = new StringBuilder();
                }
                elementsList.add(aChar + "");
            }
        }
        if (!s.toString().equals("")) {
            elementsList.add(s.toString());
        }
        Element[] elements = new Element[elementsList.size()];
        for (int n = 0; n < elementsList.size(); n++) {
            elements[n] = new Element(elementsList.get(n));
        }

        for (Element nextElement : elements) {
            if (nextElement.getType() == Element.DIGIT) {
                VALUE_STACK.addElement(nextElement);
            } else if (nextElement.getType() == Element.MATH_SYMBOL) {
                if (!OPERATOR_STACK.isEmpty() && nextElement.getPrecedence() <= OPERATOR_STACK.getTop().getPrecedence()) {
                    while (!OPERATOR_STACK.isEmpty() && nextElement.getPrecedence() <= OPERATOR_STACK.getTop().getPrecedence()) {
                        Element currentOperator = OPERATOR_STACK.getTop();
                        OPERATOR_STACK.removeElement();
                        intermediateCalculation(currentOperator);
                    }
                }
                OPERATOR_STACK.addElement(nextElement);
            } else if (nextElement.getType() == Element.LEFT_PARENTHESIS) {
                OPERATOR_STACK.addElement(nextElement);
            } else if (nextElement.getType() == Element.RIGHT_PARENTHESIS) {
                while (!OPERATOR_STACK.isEmpty() && OPERATOR_STACK.getTop().getType() == Element.MATH_SYMBOL) {
                    Element currentOperator = OPERATOR_STACK.getTop();
                    OPERATOR_STACK.removeElement();
                    intermediateCalculation(currentOperator);
                }
                if (!OPERATOR_STACK.isEmpty() && OPERATOR_STACK.getTop().getType() == Element.LEFT_PARENTHESIS) {
                    OPERATOR_STACK.removeElement();
                } else {
                    System.out.println("incomplete pair of parenthesis");
                    error = true;
                }
            } else if (nextElement.getType() == Element.INVALID) {
                System.out.println("invalid value");
                error = true;
            // if you think that space is also invalid next if-else block should be deleted or commented out
            } else if (nextElement.getType() == Element.SPACE) {
                continue;
            }

        }

        while (!OPERATOR_STACK.isEmpty() && OPERATOR_STACK.getTop().getType() == Element.MATH_SYMBOL) {
            Element toProcess = OPERATOR_STACK.getTop();
            OPERATOR_STACK.removeElement();
            intermediateCalculation(toProcess);
        }

        if (!error) {
            Element result = VALUE_STACK.getTop();
            VALUE_STACK.removeElement();
            if (!OPERATOR_STACK.isEmpty() || !VALUE_STACK.isEmpty()) {
                return null;
            }
            if (result.getValue() % 1 == 0) {
                return (int) (result.getValue()) + "";
            }
            BigDecimal bd = new BigDecimal(Double.toString(result.getValue()));
            bd = bd.setScale(4, RoundingMode.HALF_UP);
            return bd.doubleValue() + "";
        }
        return null;
    }
}
